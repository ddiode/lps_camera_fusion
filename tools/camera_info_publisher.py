#!/usr/bin/env python

import rospy
from sensor_msgs.msg import CameraInfo
# import image_transport

#data in yaml file
#image_width: 640
#image_height: 480
#camera_name: rgb_PS1080_PrimeSense
#camera_matrix:
#  rows: 3
#  cols: 3
#  data: [548.686957935381, 0, 315.357675569088, 0, 550.96175423948, 244.269316958139, 0, 0, 1]
# distortion_model: plumb_bob
#distortion_coefficients:
#  rows: 1
#  cols: 5
#  data: [0.0516850563163864, -0.123284114109982, -0.00175517393671493, 0.00281057637882476, 0]
#rectification_matrix:
#  rows: 3
#  cols: 3
#  data: [1, 0, 0, 0, 1, 0, 0, 0, 1]
#projection_matrix:
#  rows: 3
#  cols: 4
#  data: [549.749145507812, 0, 316.358160955788, 0, 0, 553.449951171875, 243.1548, 0, 0, 0, 1, 0]

# see https://svn.personalrobotics.ri.cmu.edu/public/13.1/moped/imagesender/imagesender.py


def main():
    cam_pub = rospy.Publisher("/camera/rgb/camera_info", CameraInfo, queue_size=10)
    cam_info = CameraInfo()
    cam_info.width = 640
    cam_info.height = 480
    cam_info.D = [0.0516850563163864, -0.123284114109982, -0.00175517393671493, 0.00281057637882476, 0]
    cam_info.K = [548.686957935381, 0, 315.357675569088, 0, 550.96175423948, 244.269316958139, 0, 0, 1]
    cam_info.R = [1, 0, 0, 0, 1, 0, 0, 0, 1]
    cam_info.P = [549.749145507812, 0, 316.358160955788, 0, 0, 553.449951171875, 243.1548, 0, 0, 0, 1, 0]
    cam_info.distortion_model = "plumb_bob"

    rospy.init_node('campinfoublisher', anonymous=True)
    rospy.loginfo("Publishing static camera info data")

    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        cam_info.header.stamp = rospy.Time.now()
        cam_pub.publish(cam_info)
        try:
            rate.sleep()
        except rospy.ROSTimeMovedBackwardsException:
            rospy.logwarn("Moved back in time! Are you playing a rosbag loop?")
            pass

if __name__ == '__main__':
    main()
