import ransac
from geometry_msgs.msg import Point32

def detect(p):
    return cluster(p)


def cluster(p):
    r = ransac.Ransac(0.005, 0.999, 0.05)
    r.fitline(p)

    outliers = r.get_outlier_index()

    # check if outliers is empty
    if not outliers:
        return [[], []]

    # cluster outliers which are connected
    clusters = []
    number_of_clusters = 0
    tmp_cluster = []
    for i in range(len(outliers)-1):
        if (outliers[i+1] - outliers[i]) == 1:
            tmp_cluster.append(outliers[i])
        else:
            tmp_cluster.append(outliers[i])
            clusters.append(list(tmp_cluster))
            number_of_clusters += 1
            tmp_cluster = []

    # handle last outlier (not handled in loop)
    if len(outliers) == 1:
        clusters.append(outliers)
        number_of_clusters = 1
    else:
        if (outliers[-1] - outliers[-2]) == 1:
            tmp_cluster.append(outliers[-1])
            clusters.append(tmp_cluster[:])
            number_of_clusters += 1
        else:
            clusters.append([outliers[-1]])
            number_of_clusters += 1

    means = []
    # calculate mean value
    for c in clusters:
        xmean = 0
        ymean = 0
        zmean = 0
        for i in c:
            xmean += p[i].x
            ymean += p[i].y
            zmean += p[i].z
        means.append(Point32(xmean/len(c), ymean/len(c), zmean/len(c)))

    # print "clustered"

    return [clusters, means]
