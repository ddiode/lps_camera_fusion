#!/usr/bin/env python

import cv2
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def plot(data):
    height = 800
    width = 600
    xmarg = 0.01  # margin left and right of the drawn picture
    ymarg = 0.01  # margin at top and bottom of the drawn picture

    image = np.zeros((height, width, 3), np.uint8)
    image[:] = (255, 255, 255)  # make image white

    x_max = 0
    x_min = 0
    y_max = 0
    y_min = 0

    # determine boundaries of data
    for p in iter(data):
        if p.x > x_max:
            x_max = p.x
        if p.x < x_min:
            x_min = p.x
        if p.z > y_max:
            y_max = p.z
        if p.z < y_min:
            y_min = p.z

    mag_x = width/float(x_max-x_min + 2*xmarg)
    mag_y = height/float(y_max-y_min + 2*ymarg)

    i = 0

    for p in iter(data):
        x = int((p.x - x_min + xmarg)*mag_x)
        y = int((p.z - y_min + ymarg)*mag_y)
        i += 1
        image[y, x] = (0, 0, 0)

    cv2.imshow('laser data', image)
    cv2.waitKey(1)


def plotcolor(data, inlier, outlier):
    height = 800
    width = 600
    xmarg = 0.01  # margin left and right of the drawn picture
    ymarg = 0.01  # margin at top and bottom of the drawn picture

    image = np.zeros((height, width, 3), np.uint8)
    image[:] = (255, 255, 255)  # make image white

    x_max = - float('inf')
    x_min = float('inf')
    y_max = - float('inf')
    y_min = float('inf')

    # determine boundaries of data
    for p in iter(data):
        if p.x > x_max:
            x_max = p.x
        if p.x < x_min:
            x_min = p.x
        if p.z > y_max:
            y_max = p.z
        if p.z < y_min:
            y_min = p.z

    mag_x = width/float(x_max-x_min + 2*xmarg)
    mag_y = height/float(y_max-y_min + 2*ymarg)

#    print "ymin:, " + str(y_min) + ", ymax: " + str(y_max)

    for i in inlier:
        p = data[i]
        x = int((p.x - x_min + xmarg)*mag_x)
        y = int((p.z - y_min + ymarg)*mag_y)

        image[y, x] = (0, 0, 0)

    for i in outlier:
        p = data[i]
        x = int((p.x - x_min + xmarg)*mag_x)
        y = int((p.z - y_min + ymarg)*mag_y)

        image[y, x] = (0, 0, 255)

    cv2.imshow('laser data', image)
    cv2.waitKey(1)



def plot3d(data, inlier, outlier):
    """ Generates a heatmap of laser data

    Don't forget to alter the size of the data at the end of the function (rosbag info file)

    :param data: point data
    :param inlier: index of inliers
    :param outlier: index of outlier
    :return:
    """
    x = []
    y = []
    for p in data:
        x.append(p.x)
        y.append(p.z)

    inl_x = []
    inl_y = []
    for i in inlier:
        inl_x.append(data[i].x)
        inl_y.append(data[i].z)

    outl_x = []
    outl_y = []
    for o in outlier:
        outl_x.append(data[o].x)
        outl_y.append(data[o].z)

#    plt.cla()
#    plt.plot(inl_x, inl_y, 'b.')
#    plt.hold(True)
#    plt.plot(outl_x, outl_y, 'r.')
#    plt.draw()
#    plt.show(block=False)

    # maximum 100 meassurements
#    if len(plot3d.inlxt) > 100:
#        plot3d.inlxt.pop(0)
#        plot3d.inlyt.pop(0)
#        plot3d.outlxt.pop(0)
#        plot3d.outlyt.pop(0)

    plot3d.xt.append(list(x))
    plot3d.yt.append(list(y))
    plot3d.inlxt.append(list(inl_x))
    plot3d.inlyt.append(list(inl_y))
    plot3d.outlxt.append(list(outl_x))
    plot3d.outlyt.append(list(outl_y))

#    fig = plt.figure('test')
#    ax = fig.gca(projection='3d')
#    plt.cla();
#    for i in range(len(plot3d.inlxt)):
#        ax.plot(inl_x, inl_y, zs=i, zdir='z')  # , 'b.'
#        ax.hold(True)
#        ax.plot(outl_x, outl_y, zs=i, zdir='z')  # , 'r.'
#    plt.draw()
#    plt.show(block=False)
#    plt.imshow(np.matrix(plot3d.yt))
#    plt.draw()
#    plt.show(block=False)


    if len(plot3d.yt) == 420:  # insert length of data stream here
        a = np.matrix(plot3d.yt)
        b = a[:, 6:370] # remove invalid points at the beginning and end
        plt.imshow(b)
        plt.draw()
        plt.show()

    return False

plot3d.xt = []
plot3d.yt = []
plot3d.inlxt = []
plot3d.inlyt = []
plot3d.outlxt = []
plot3d.outlyt = []

