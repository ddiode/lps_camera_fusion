#!/usr/bin/env python

import rospy
# import roslib
import cv2
import copy
import bisect # bisection search
# from std_msgs.msg import String
from sensor_msgs.msg import Image
from sensor_msgs.msg import PointCloud
from sensor_msgs.msg import CameraInfo
from geometry_msgs.msg import Point32
from cv_bridge import CvBridge, CvBridgeError
import image_geometry
import tf

import plot_lps_data
import ransac
import detect_objects


global_images_buflen = 10
global_image_times = []
global_images = []

global_lasers_buflen = 30
global_laser_times = []
global_lasers = []

def lps_camera_fusion():
    rospy.init_node('lps_camera_fusion')
    # startup messages
    rospy.loginfo("lps to image fusion")

    # add subscriber
    rospy.Subscriber("/camera/rgb/image_raw", Image, image_callback)
    rospy.Subscriber("/lps36/pointcloud", PointCloud, laser_callback)

    rate = rospy.Rate(30)

    rospy.loginfo("Waiting for valid camera info")
    camera_info = rospy.wait_for_message("/camera/rgb/camera_info", CameraInfo)
    rospy.loginfo("Camera info OK!")

    cam_model = image_geometry.PinholeCameraModel()
    cam_model.fromCameraInfo(camera_info)

    # for transforming in camera coordinate system
    transform_listener = tf.TransformListener()

    # for generating OpenCV image from ros image message
    bridge = CvBridge()

    while not rospy.is_shutdown():
        if (len(global_images) > 0) & (len(global_lasers) > 0):
            # take first image in que and do nearest neighbor search for finding corresponding laser scan
            imagetime = global_image_times[0]
            image = global_images[0]
            laser_index_left = bisect.bisect_left(global_laser_times, imagetime)
            try:
                laser_left_time = global_laser_times[laser_index_left]
            except IndexError:
                # rospy.logerr("index out of bound: laser_index_left=%d but len(global_laser_times)=%d! Skipping frame!, (Negative time ump?)", laser_index_left, len(global_laser_times))
                continue

            if laser_index_left < len(global_lasers)-1:
                laser_right_time = global_laser_times[laser_index_left+1]
            else:
                laser_right_time = global_laser_times[laser_index_left]

            if abs(imagetime - laser_left_time) < abs(imagetime -laser_right_time):
                pc = copy.deepcopy(global_lasers[laser_index_left])
            else:
                pc = copy.deepcopy(global_lasers[laser_index_left+1])

            # use ransac and transform into line
            points_ransac = copy.deepcopy(pc.points)
            # r = ransac.Ransac(0.002, 0.999, 0.05)
            # r.fitline(points_ransac)
            # r.transform(points_ransac)

            cm = detect_objects.detect(points_ransac)

            # process only if outlying points are found
            if len(cm) > 0:
                clusters = cm[0]
                means = []

                # calculate means of clusters, this is necessary, because we need the not transformed (ransac)
                # mean value
                for c in clusters:
                    xmean = 0
                    ymean = 0
                    zmean = 0
                    for i in c:
                        xmean += pc.points[i].x
                        ymean += pc.points[i].y
                        zmean += pc.points[i].z
                    means.append(Point32(xmean/len(c), ymean/len(c), zmean/len(c)))
                means_pointcloud = PointCloud()
                means_pointcloud.header = pc.header  # header must be the same for frame information
                means_pointcloud.points = means

                try:
                    means_pointcloud_transformed = transform_listener.transformPointCloud("/camera_rgb_optical_frame", means_pointcloud)
                except tf.ExtrapolationException:
                    rospy.loginfo("Transformation lookup failed (time beyond limits), skipping frame")
                    continue
                except tf.LookupException:
                    rospy.loginfo("Transformation lookup failed (frame not found), skipping frame")
                    continue

                means_uv = []
                for p in means_pointcloud_transformed.points:
                    uv = cam_model.project3dToPixel((p.x, p.y, p.z))
                    if (uv[0] < cam_model.width) & (uv[1] < cam_model.height):
                        means_uv.append((int(uv[0]+0.5), int(uv[1]+0.5)))  # round to int

                # ros to opencv image
                cv_image = bridge.imgmsg_to_cv2(image, "bgr8")

                for i in range(len(means_uv)):
                    #  u = means_uv[i][1]
                    #  v = means_uv[i][2]
                    cv2.circle(cv_image, means_uv[i], 5, (255, 0, 0))

                cv2.imshow("BGR Data", cv_image)
                cv2.waitKey(1)
        try:
            rate.sleep()
        except rospy.ROSTimeMovedBackwardsException:
            rospy.loginfo("Sleep failed. Time jump?")
            pass


def image_callback(im):
    assert isinstance(im, Image)

    if image_callback.showimage:
        show_image(im)
        rospy.loginfo("Image %d.%d", im.header.stamp.secs, im.header.stamp.nsecs)

    if image_callback.printtime:
        time = im.header.stamp.secs*1000000000 + im.header.stamp.nsecs
        deltat = time - image_callback.oldtime
        image_callback.oldtime = time
        rospy.loginfo("DeltaT Image [ms]= %f", deltat/1000000.)

    global_images.append(copy.deepcopy(im))
    global_image_times.append(im.header.stamp.secs*1000000000 + im.header.stamp.nsecs)
    if len(global_images) > global_images_buflen:
        global_images.pop(0)
        global_image_times.pop(0)

# static (persistent) variables declaration for image_callback
image_callback.oldtime = 0
image_callback.printtime = False
image_callback.showimage = False

def laser_callback(pc):
    assert isinstance(pc, PointCloud)
    # rospy.loginfo("PointCloud %d.%d", pc.header.stamp.secs, pc.header.stamp.nsecs)

    points = pc.points

    # remove invalid points at the beginning and the end of the measurement data
    if laser_callback.remove_trailing_leading_invalids:
        for i in range(0, len(points), 1):
            p = pc.points[0]  # take always leading element
            if p.x == 0 and p.y == 0 and p.z == 0:
                pc.points.pop(0)
            else:
                break

        for i in range(0, len(points), 1):
            p = pc.points[-1]  # take always last element of list
            if p.x == 0 and p.y == 0 and p.z == 0:
                pc.points.pop(-1)
            else:
                break

    global_lasers.append(pc)
    global_laser_times.append(pc.header.stamp.secs*1000000000 + pc.header.stamp.nsecs)
    if len(global_lasers) > global_lasers_buflen:
        global_lasers.pop(0)
        global_laser_times.pop(0)

    if laser_callback.plot2d or laser_callback.plot3d:
        points = pc.points
        # use ransac and transform into line (just for plotting)
        points_ransac = copy.deepcopy(points)
        r = ransac.Ransac(0.002, 0.999, 0.05)
        r.fitline(points_ransac)
        r.transform(points_ransac)
        detect_objects.detect(points_ransac)

    if laser_callback.plot2d:
        plot_lps_data.plot(pc.points)
        plot_lps_data.plotcolor(pc.points, r.get_inliers_index(), r.get_outlier_index())

    if laser_callback.plot3d:
        plot_lps_data.plot3d(pc.points, r.get_inliers_index(), r.get_outlier_index())

    if laser_callback.printtime:
        time = pc.header.stamp.secs*1000000000 + pc.header.stamp.nsecs
        deltat = time - laser_callback.oldtime
        laser_callback.oldtime = time
        rospy.loginfo("DeltaT Laser [ms]= %f", deltat/1000000.)

# static variables declaration for laser_callback
laser_callback.oldtime = 0
laser_callback.remove_trailing_leading_invalids = False
laser_callback.plot3d = False
laser_callback.plot2d = False
laser_callback.printtime = False


def show_image(im):
    bridge = CvBridge()
    cv_image = bridge.imgmsg_to_cv2(im, "bgr8")

    cv2.imshow("BGR Data", cv_image)
    cv2.waitKey(1)

# main function call
if __name__ == '__main__':
    lps_camera_fusion()