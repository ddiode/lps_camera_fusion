#!/usr/bin/env python
import roslib
import rospy
import tf


if __name__ == '__main__':
    rospy.init_node('lps_tf_broadcaster')
    rospy.loginfo("Broadcasting fixed lps frame")
    br = tf.TransformBroadcaster()
    rate = rospy.Rate(100.0)
    while not rospy.is_shutdown():
        # parameters from manual calibration
        offset = (0.21, 0.12, 0.575)
        rotation = tf.transformations.quaternion_from_euler(-0.05, 0.0, -2.88, 'rzyx')
        br.sendTransform(offset, rotation, rospy.Time.now(), "lps_frame", "world")
        try:
            rate.sleep()
        except rospy.ROSTimeMovedBackwardsException:
            rospy.logwarn("Moved back in time! Are you playing a rosbag loop?")
            pass
