#!/usr/bin/env python

import random
import math
import numpy as np
from sensor_msgs.msg import PointCloud


class Ransac:
    def __init__(self, inlier_margin, confidence, min_sample_distance):
        self.inlier_margin = inlier_margin
        self.confidence = confidence
        self.min_sample_distance = min_sample_distance
        self.inliers = []
        self.outliers = []
        self.pk = 0
        self.pd = 0
        self.m = 2  # model dimensions

    def fitline(self, p):
        assert isinstance(p, list)

        etha = 1.  # model error probability
        epsilon = 0.
        random.seed()
        k = 1

        while etha > (1-self.confidence):
            sdist = 0
            while sdist < self.min_sample_distance:
                p1_index = random.randint(0, len(p)-1)
                p2_index = random.randint(0, len(p)-1)
                p1 = p[p1_index]
                p2 = p[p2_index]
                sdist = math.sqrt((p1.x-p2.x)**2 + (p1.y-p2.y)**2 + (p1.z-p2.z)**2)

            pk_new = (p1.z-p2.z)/(p1.x-p2.x)
            pd_new = p1.z - pk_new * p1.z

            inliers_new_index = []
            outliers_new_index = []

            i = 0
            for vec in p:  # calculate inliers
                if abs(pk_new * vec.x + pd_new - vec.z) <= self.inlier_margin:
                    inliers_new_index.append(i)
                else:
                    outliers_new_index.append(i)

                i += 1

            if len(self.inliers) < len(inliers_new_index):
                self.inliers = list(inliers_new_index)  # wired syntax for copying list
                self.outliers = list(outliers_new_index)
                epsilon = len(inliers_new_index)/float(len(p))
                self.pk = pk_new
                self.pd = pd_new
            etha = (1 - (epsilon**self.m))**k
            k=k+1

        # print "ransac iterations: " + str(k) + " inliers: " + str(len(self.inliers))

        # least squares fitting of parameters
        # this is done by calculating the soultion of the equation
        # A*x = b
        # where A contains ones and the values of x direction and b contains the values in z direction

        ls_x = np.array([])
        ls_z = np.array([])
        for i in self.inliers:
            ls_x = np.append(ls_x, p[i].x)
            ls_z = np.append(ls_z, p[i].z)

        aa = np.vstack([ls_x, np.ones(len(ls_x))]).T
        tmp = np.linalg.lstsq(aa, ls_z)
        self.pk = tmp[0][0]
        self.pd = tmp[0][1]
        # self.pk, self.pd = np.linalg.lstsq(aa, ls_z)

        # calculate inliers and outliers again
        self.inliers = []
        self.outliers = []
        i = 0
        for vec in p:  # calculate inliers
            if abs(self.pk * vec.x + self.pd - vec.z) <= self.inlier_margin:
                self.inliers.append(i)
            else:
                self.outliers.append(i)
            i += 1

    def get_d(self):
        return self.pd

    def get_k(self):
        return self.pk

    def get_inliers_index(self):
        return list(self.inliers)  # wired syntax for copy of list

    def get_outlier_index(self):
        return list(self.outliers)

    def transform(self, points):
        for p in points:
            p.z = - p.z + self.pk * p.x + self.pd